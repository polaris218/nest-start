import { Controller, Get, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express'

@Controller('users')
export class UsersController {
    @Get()
    findAll(@Req() request: Request): string {
        return 'Get all Users'
    }
}
